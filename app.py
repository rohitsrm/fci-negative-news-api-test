from flask import Flask
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import text
from database_setup import Base
import os

app = Flask(__name__)
#app.config["SERVER_NAME"] = "sentinel.eastus.cloudapp.azure.com"
app.config["APPLICATION_ROOT"] = os.environ.get('SERVER_CONTEXT_PATH','/')
APPLICATION_ROOT = os.environ.get('SERVER_CONTEXT_PATH','/')

@app.route(APPLICATION_ROOT)
@app.route(APPLICATION_ROOT + 'hello')
def HelloWorld():
    engine = create_engine('mssql+pyodbc://sqladmin:FinCrimeDEV2017$@adcfincrimeops.database.windows.net:1433/adc-ci?driver=ODBC+Driver+17+for+SQL+Server')
    Base.metadata.bind = engine
    DBSession = sessionmaker(bind = engine)
    session = DBSession()

    sql = text('SELECT name FROM script')
    result = engine.execute(sql)
    names = []
    for row in result:
        names.append(row[0])

    file = open("/data/ci/files/testfile.txt","w")
    file.write("Hello World")
    file.close()

    return ','.join(names)

    if __name__ == '__main__':
        app.debug = True
        app.run(debug=True, host='0.0.0.0')
        