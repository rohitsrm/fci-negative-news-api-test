FROM ubuntu:17.10

ENV LANG=C.UTF-8
ENV ACCEPT_EULA=Y

# Add unprivileged user
RUN useradd --user-group --create-home --shell /bin/false app

RUN apt-get update -y

RUN apt-get install -qy --no-install-recommends \
  python-pip \
  python-dev \
  build-essential \
  python3 \
  python3-pip \
  python3-dev \
  python3-venv \
  python3-setuptools \
  python3-reportlab \
  python3-pil \
  python3-wheel \
  curl

RUN pyvenv /appenv \
  && pyvenv --system-site-packages /appenv

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - 
RUN touch /etc/apt/sources.list.d/mssql-release.list
RUN curl https://packages.microsoft.com/config/ubuntu/17.10/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN apt-get install -qy --no-install-recommends \ 
  msodbcsql17 \
  mssql-tools \
  unixodbc-dev 

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

RUN apt-get autoremove -y && apt-get clean -y
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /root/*

COPY app.py database_setup.py ./
ENV FLASK_APP=app.py
USER app
EXPOSE 5000

ENTRYPOINT gunicorn -b 0.0.0.0:5000 --access-logfile - "app:app" 
